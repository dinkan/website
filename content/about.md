---
title: "about"
date: "2022-11-13"
layout: "about"
---

structnull(1)                                


**NAME:**  
**structnull/adharsh** — The personal website of Adharsh, a person who has musings for FOSS, Linux, anime, cinema, sonic art, and all things technology.

**SYNOPSIS:**  
structnull [OPTIONS]

**DESCRIPTION:**  
(V A) Adharsh — a tech enthusiast who ponders life's mysteries and codes ideas into reality(mostly).

**DETAILS:**  
**Built with:** [Hugo](https://gohugo.io)  
**Source Code:** Hosted at [Codeberg](https://codeberg.org/dinkan/website)

**CONTACT:**  
[**Linkdin**](https://www.linkedin.com/in/adharsh-va-9432592ab/) || [**Instagram** ](https://www.instagram.com/va_adharsh)  || [**GitHub** ](https://github.com/structnull)  || [**Email**](adharshva321@gmail.com)

**COPYRIGHT:**  
© 2020-25 Adharsh. All rights reserved.  
This website is free software—redistribution is permitted under the GPLv2.
