---
title: "Nvim on immutable distro/environment"
date: 2023-04-27T00:30:26+05:30
---

what is dis ? ==> le script to install nvim appimage on a distro without sudo access

why? ==> coz my lab teacher wont give me sudo access

what does this do? ==> curls the nvim binary and install it in $HOME/bin/ and configures a sane init.vim file

## LE SCRIPT
```
#!/bin/bash

# Define the URL to download the latest stable release of Neovim for Linux.
NEOVIM_URL="https://github.com/neovim/neovim/releases/latest/download/nvim.appimage"

# Define the path where you want to store the Neovim executable.
NEOVIM_PATH="$HOME/bin/nvim"

# Define the path where you want to store the Neovim configuration file.
NEOVIM_CONFIG_PATH="$HOME/.config/nvim/init.vim"

# Create the directories if they don't exist.
if [ ! -d "$HOME/bin" ]; then
    mkdir "$HOME/bin"
fi

if [ ! -d "$HOME/.config/nvim" ]; then
    mkdir -p "$HOME/.config/nvim"
fi # Download the latest stable release of Neovim to the specified path.
curl -L "$NEOVIM_URL" -o "$NEOVIM_PATH"

# Make the downloaded file executable.
chmod +x "$NEOVIM_PATH"

# Add the directory containing the Neovim executable to your PATH environment variable.
echo 'export PATH="$HOME/bin:$PATH"' >> ~/.bashrc

# Source your .bashrc file to apply the changes.
source ~/.bashrc

# Create an alias for neovim so that it can be invoked by typing "nvim" in the terminal.
echo 'alias nvim="$HOME/bin/nvim"' >> ~/.bashrc

# Create the Neovim configuration file with some quality-of-life defaults and settings.
cat << EOF > "$NEOVIM_CONFIG_PATH"
" Quality-of-life settings
set number
set relativenumber
set cursorline
set showmode
set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

" Appearance
set termguicolors " enable true color support
set background=dark " set dark mode by default

" Cursor and scrolling
set scrolloff=8 " keep 8 lines of context when scrolling
set sidescrolloff=8 " keep 8 columns of context when scrolling horizontally
set cursorlineopt=both " highlight both the current line and column
set whichwrap+=<,>,h,l " allow horizontal scrolling in addition to vertical scrolling

" Search
set hlsearch " highlight search matches
set incsearch " highlight search matches as you type
set ignorecase " case-insensitive searching
set smartcase " case-sensitive searching if query contains uppercase characters

" Miscellaneous
set hidden " allows you to switch buffers without saving first
set laststatus=2 " always show status line
set showtabline=2 " always show tab line
set mouse=a " enable mouse support
set completeopt=menuone,noinsert,noselect " nicer completion menu

" Syntax highlighting
syntax enable

" Clipboard integration (requires xclip)
set clipboard+=unnamedplus
EOF

echo "Neovim has been successfully installed and configured. You can now use it by typing 'nvim' in the terminal."
```


## The wget version -->

```
#!/bin/bash

NEOVIM_URL="https://github.com/neovim/neovim/releases/latest/download/nvim.appimage"

NEOVIM_PATH="$HOME/bin/nvim"

NEOVIM_CONFIG_PATH="$HOME/.config/nvim/init.vim"

if [ ! -d "$HOME/bin" ]; then
    mkdir "$HOME/bin"
fi

if [ ! -d "$HOME/.config/nvim" ]; then
    mkdir -p "$HOME/.config/nvim"
fi 

wget "$NEOVIM_URL" -O "$NEOVIM_PATH"

chmod +x "$NEOVIM_PATH"

echo 'export PATH="$HOME/bin:$PATH"' >> ~/.bashrc

source ~/.bashrc

echo 'alias nvim="$HOME/bin/nvim"' >> ~/.bashrc

cat << EOF > "$NEOVIM_CONFIG_PATH"
" Quality-of-life settings
set number
set relativenumber
set cursorline
set showmode
set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

" Appearance
set termguicolors " enable true color support
set background=dark " set dark mode by default

" Cursor and scrolling
set scrolloff=8 " keep 8 lines of context when scrolling
set sidescrolloff=8 " keep 8 columns of context when scrolling horizontally
set cursorlineopt=both " highlight both the current line and column
set whichwrap+=<,>,h,l " allow horizontal scrolling in addition to vertical scrolling

" Search
set hlsearch " highlight search matches
set incsearch " highlight search matches as you type
set ignorecase " case-insensitive searching
set smartcase " case-sensitive searching if query contains uppercase characters

" Miscellaneous
set hidden " allows you to switch buffers without saving first
set laststatus=2 " always show status line
set showtabline=2 " always show tab line
set mouse=a " enable mouse support
set completeopt=menuone,noinsert,noselect " nicer completion menu

" Syntax highlighting
syntax enable

" Clipboard integration (requires xclip)
set clipboard+=unnamedplus
EOF

echo "Neovim has been successfully installed and configured. You can now use it by typing 'nvim' in the terminal."
```
