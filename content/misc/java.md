---
title: "Java"
date: 2024-01-14T13:14:43+05:30
---

-   Java Library - String Handling:
    -   [String Constructors](https://www.javastring.net/java/string/java-string-constructors)
    -   [String Length, Special String Operations - Character Extraction, String Comparison, Searching Strings, Modifying Strings, using valueOf()](https://www.codingninjas.com/studio/library/special-string-operations-in-java)
    -   [Comparison of StringBuffer and String](https://www.tutorialkart.com/java/java-string-class-constructors-methods/)
-   Collections framework:
    -   [Collections overview](https://docs.oracle.com/javase/8/docs/technotes/guides/collections/overview.html)
    -   [Collections Interfaces- Collection Interface, List Interface](https://docs.oracle.com/javase/tutorial/collections/interfaces/summary.html)
-   Collections Class:
    -   [ArrayList class](https://www.javatpoint.com/java-arraylist)
    -   [Accessing a Collection via an Iterator](http://www.btechsmartclass.com/java/java-Iterator-class.html)
-   Event handling:
    -   [Event Handling Mechanisms, Delegation Event Model, Event Classes, Sources of Events, Event Listener Interfaces, Using the Delegation Model](https://www.developer.com/project-management/understanding-and-using-the-java-delegation-event-model/)
-   Multithreaded Programming:
    -   [The Java Thread Model, The Main Thread](https://www.geeksforgeeks.org/main-thread-java/)
    -   [Creating Thread, Creating Multiple Threads, Synchronization, Suspending, Resuming and Stopping Threads](https://www.geeksforgeeks.org/java-threading-programs/)

## cherry picked questions -->

1.  What is the difference between AWT and Swing?

    AWT (Abstract Window Toolkit) and Swing are both used to develop GUI-based applications in Java, but they have some key differences:

    1. **API Package**: AWT component classes are provided by the `java.awt` package, while Swing component classes are provided by the `javax.swing` package¹².
    2. **Operating System Dependency**: AWT components are mainly dependent on the operating system, whereas Swing components are not. Swing is completely scripted in Java¹².
    3. **Weightiness**: AWT is considered heavyweight since it uses the resources of the operating system. On the other hand, Swing is mostly lightweight since it doesn't need any operating system object for processing¹².
    4. **Appearance**: The appearance of AWT components is mainly not configurable and generally depends on the operating system's look and feel. In contrast, Swing components are configurable and support pluggable look and feel².
    5. **Number of Components**: AWT provides a smaller number of components compared to Swing. Swing provides a greater number of components such as list, scroll panes, tables, color choosers, etc².
    6. **Functionality and Implementation**: AWT serves as a thin layer of development on top of the OS, and many features need to be completely developed by the developer. In contrast, Swing components provide higher-level inbuilt functions for the developer that facilitate less code writing².
    7. **Memory and Speed**: AWT needs a higher amount of memory for execution and is slower than Swing in terms of performance. Swing needs less memory space compared to AWT and is faster².

    In summary, while AWT is Java's original platform-dependent windowing, graphics, and user interface widget toolkit, Swing is a GUI widget toolkit for Java that is an extension of AWT³⁵. Swing also has richer functionality, and for GUI-intensive work, AWT can feel primitive compared to Swing⁴.
2.  what does this keyword do ?

    In Java, the `this` keyword is a reference variable that refers to the current object. It is used to represent the instance of the class in which it appears. Here are some common uses of the `this` keyword:

    1. **To distinguish instance variables from local variables:**
       ```java
       class Example {
           int x;
    
           void setX(int x) {
               // Use 'this' to refer to the instance variable 'x'
               this.x = x;
           }
       }
       ```
    
       In the above example, `this.x` refers to the instance variable `x`, while `x` refers to the local variable passed as a parameter.
    
    2. **To invoke the current object's method:**
       ```java
       class Example {
           void display() {
               System.out.println("Inside display method");
           }
    
           void callDisplay() {
               // Use 'this' to invoke the current object's method
               this.display();
           }
       }
       ```
    
       In the `callDisplay` method, `this.display()` is equivalent to `display()`.
    
    3. **To pass the current object as a parameter to other methods:**
       ```java
       class Example {
           void processObject(Example obj) {
               // ...
           }
    
           void someMethod() {
               // Use 'this' to pass the current object to another method
               processObject(this);
           }
       }
       ```
    
       Here, `this` is passed as an argument to the `processObject` method.
    
    4. **In constructors to call another constructor in the same class:**
       ```java
       class Example {
           int x;
           int y;
    
           Example() {
               // Call another constructor in the same class using 'this'
               this(0, 0);
           }
    
           Example(int x, int y) {
               this.x = x;
               this.y = y;
           }
       }
       ```
    
    In the above example, the `Example()` constructor calls another constructor using `this(0, 0)`.
    The `this` keyword is especially useful in situations where instance variables and parameters have the same name, helping to differentiate between the two.
   

### Primitive Data Types
1. **What are the primitive data types in Java?**
   
   Java has four primitive data types:
   ```java
   int, float, char, boolean
   ```

2. **Explain the difference between int and float data types.**

   The `int` data type is used for representing integers, while the `float` data type is used for representing floating-point numbers, which can have decimal parts.

3. **What is a literal in Java?**

   A literal in Java is a representation of a fixed value in the source code. For example:
   ```java
   int number = 42; // Here, 42 is a literal.
   ```

4. **How can you convert a float to an int in Java?**

   You can convert a float to an int using type casting:
   ```java
   float floatNumber = 3.14f;
   int intNumber = (int) floatNumber;
   ```

### Operators
5. **List and briefly explain the different types of operators in Java.**

   - Arithmetic Operators: Perform basic arithmetic operations.
   - Bitwise Operators: Perform bit-level operations.
   - Relational Operators: Compare values.
   - Boolean Logical Operators: Perform logical operations.
   - Assignment Operator: Assigns a value to a variable.
   - Conditional (Ternary) Operator: A shorthand for if-else statements.

6. **What is the difference between '==' and 'equals()' in Java?**

   The `==` operator checks for reference equality, i.e., whether two objects refer to the same memory location. The `equals()` method is used for content equality, and it needs to be explicitly overridden in classes where content comparison is meaningful.

### Control Statements
7. **Differentiate between selection and iteration statements in Java.**

   - Selection Statements: Used for decision-making (e.g., if, switch).
   - Iteration Statements: Used for loop execution (e.g., for, while, do-while).

8. **Explain the purpose of the 'break' statement in Java.**

   The `break` statement is used to terminate the loop or switch statement it is in. It is often used to exit a loop prematurely based on a certain condition.

### Object-Oriented Programming (OOP) in Java
9. **What is the significance of the 'this' keyword in Java?**

   The `this` keyword is a reference to the current object. It is used to differentiate instance variables from local variables when they have the same name.

10. **Explain the concept of method overloading in Java.**

    Method overloading allows a class to have multiple methods with the same name but different parameter lists (different types or different number of parameters).


### Inheritance
12. **Explain the concept of a "protected" member in Java. Provide an example of its usage.**

    The `protected` access modifier allows members (fields and methods) to be accessed within the same package and by subclasses outside the package. Example:

    ```java
    package com.example;

    public class Parent {
        protected int protectedField;

        protected void protectedMethod() {
            // Method implementation
        }
    }
    ```

    In a subclass within the same package:

    ```java
    package com.example;

    public class Child extends Parent {
        public void accessProtected() {
            protectedField = 42;       // Accessing protected field
            protectedMethod();          // Accessing protected method
        }
    }
    ```

### Abstract Classes and Interfaces
13. **Compare and contrast abstract classes and interfaces in Java. Provide a scenario where you would prefer using an abstract class over an interface.**

    - **Abstract Classes:**
        - Can have both abstract and concrete methods.
        - Can have instance variables (fields).
        - Can have constructors.
        - Support single inheritance.

    - **Interfaces:**
        - Can only have abstract methods until Java 8 (default methods added in Java 8).
        - Can't have instance variables (fields) until Java 9 (static and default methods allowed).
        - No constructors.
        - Support multiple inheritance.

    **Scenario:**
    Use an abstract class when you have a base class with common functionality and want to provide a default implementation for some methods. Use an interface when you want to enforce a contract for multiple unrelated classes.

### Exception Handling
14. **What is the purpose of the `throws` clause in a Java method signature? Provide an example.**

    The `throws` clause is used to declare exceptions that a method might throw. It informs the caller of the method about the potential exceptions it needs to handle or propagate.

    Example:

    ```java
    public class Example {
        public void someMethod() throws CustomException {
            // Method implementation that may throw CustomException
        }
    }

    class CustomException extends Exception {
        // Custom exception class
    }
    ```

### Advanced Topics
15. **Explain the concept of functional interfaces in Java. Provide an example of a lambda expression using a functional interface.**

    A functional interface is an interface with exactly one abstract method. It can have multiple default or static methods. Functional interfaces are often used in conjunction with lambda expressions to enable concise expression of instances.

    Example:

    ```java
    @FunctionalInterface
    interface MathOperation {
        int operate(int a, int b);
    }

    public class LambdaExample {
        public static void main(String[] args) {
            MathOperation addition = (a, b) -> a + b;
            System.out.println(addition.operate(5, 3));  // Output: 8
        }
    }
    ```

### Advanced Object-Oriented Programming (OOP) Concepts
16. **Explain the concept of composition in Java. Provide an example of a class utilizing composition.**

    Composition is a design technique where a class includes objects of other classes, rather than inheriting from them. This promotes code reusability and flexibility.

    Example:

    ```java
    public class Engine {
        public void start() {
            // Implementation of starting the engine
        }
    }

    public class Car {
        private Engine engine;

        public Car(Engine engine) {
            this.engine = engine;
        }

        public void startCar() {
            engine.start();
            // Other car-specific operations
        }
    }
    ```

### Java Streams and Functional Programming
17. **What are Java Streams, and how are they used for functional programming? Provide an example.**

    Java Streams provide a concise and functional approach to processing collections. They allow operations to be performed on elements in a declarative manner.

    Example:

    ```java
    import java.util.Arrays;
    import java.util.List;

    public class StreamExample {
        public static void main(String[] args) {
            List<String> words = Arrays.asList("apple", "banana", "orange", "grape", "peach");

            long count = words.stream()
                             .filter(word -> word.length() > 5)
                             .count();

            System.out.println("Number of words with length > 5: " + count);
        }
    }
    ```

### Concurrency in Java
18. **Explain the concept of thread safety in Java. Provide an example of ensuring thread safety.**

    Thread safety ensures that a piece of code or an object can be safely used by multiple threads without causing unexpected behavior. Synchronization is often used to achieve thread safety.

    Example:

    ```java
    public class Counter {
        private int count = 0;

        public synchronized void increment() {
            count++;
        }

        public int getCount() {
            return count;
        }
    }
    ```
### Packages and Interfaces

19. **Discuss the different access protection levels in Java. Provide an example demonstrating the use of access modifiers.**

    Access modifiers (`public`, `private`, `protected`, and default) control the visibility of classes, methods, and fields.

    Example:

    ```java
    // MyClass.java
    public class MyClass {
        public int publicVar;
        private int privateVar;
        protected int protectedVar;
        int defaultVar;

        public void publicMethod() {
            // Method implementation
        }

        private void privateMethod() {
            // Method implementation
        }
    }
    ```

### Exception Handling
20. **Explain the purpose of the `finally` block in Java exception handling. Provide an example where the `finally` block is crucial.**

    The `finally` block is used to ensure that a specific piece of code is always executed, whether an exception is thrown or not. It is crucial for cleanup operations.

    Example:

    ```java
    public class FileProcessor {
        public void processFile(String fileName) {
            try {
                // Code to read and process the file
                // ...
            } catch (IOException e) {
                // Handle the exception
                // ...
            } finally {
                // Close resources, like file streams, irrespective of exceptions
                // This block always executes
            }
        }
    }
    ```

### Input/Output
21. **Explain the concept of Object Streams and Serialization in Java. Provide an example of serializing and deserializing an object.**

    Serialization is the process of converting an object into a byte stream, and deserialization is the reverse process.

    Example:

    ```java
    import java.io.*;

    class Person implements Serializable {
        String name;
        int age;

        // Constructor and other methods...

        public static void main(String[] args) throws IOException, ClassNotFoundException {
            // Serialize object
            Person person = new Person("John", 25);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("person.ser"));
            objectOutputStream.writeObject(person);
            objectOutputStream.close();

            // Deserialize object
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("person.ser"));
            Person deserializedPerson = (Person) objectInputStream.readObject();
            objectInputStream.close();
        }
    }
    ```

22. **Discuss the differences between the `PrintWriter` class and the `FileWriter` class in Java I/O. Provide an example illustrating their usage.**

    `PrintWriter` and `FileWriter` are both classes used for writing data to files, but they have different functionalities.

    Example:

    ```java
    import java.io.*;

    public class FileWriterVsPrintWriter {
        public static void main(String[] args) throws IOException {
            // Using FileWriter
            FileWriter fileWriter = new FileWriter("file.txt");
            fileWriter.write("Using FileWriter");
            fileWriter.close();

            // Using PrintWriter
            PrintWriter printWriter = new PrintWriter("file.txt");
            printWriter.println("Using PrintWriter");
            printWriter.close();
        }
    }
    ```

23. **Explain the concept of CLASSPATH in Java. How does it affect package and class visibility? Provide an example.**

    The CLASSPATH is an environment variable that tells the Java Virtual Machine (JVM) where to look for user-defined classes and packages. It affects the visibility of classes and packages during compilation and execution.

    Example:

    ```java
    // MyClass.java in package com.example
    package com.example;

    public class MyClass {
        public void printMessage() {
            System.out.println("Hello from MyClass!");
        }
    }
    ```

    If CLASSPATH is set to include the current directory, you can compile and run as follows:

    ```bash
    javac com/example/MyClass.java
    java com.example.MyClass
    ```
### Swing Fundamentals
24. **Explain the Model-View-Controller (MVC) design pattern in the context of Swing. How does Swing implement MVC? Provide an example.**

    In Swing, MVC is a design pattern used to separate the application logic into three interconnected components: Model (data and business logic), View (presentation and UI components), and Controller (handles user input and updates the Model and View).

    Example:

    ```java
    import javax.swing.*;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;

    // Model
    class CalculatorModel {
        private int result;

        public void add(int value) {
            result += value;
        }

        public int getResult() {
            return result;
        }
    }

    // View
    class CalculatorView extends JFrame {
        private JTextField textField;
        private JButton addButton;

        public CalculatorView() {
            textField = new JTextField(10);
            addButton = new JButton("Add");

            setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
            add(textField);
            add(addButton);
        }

        public int getUserInput() {
            return Integer.parseInt(textField.getText());
        }

        public void setResult(int result) {
            textField.setText(String.valueOf(result));
        }

        public void addAddButtonListener(ActionListener listener) {
            addButton.addActionListener(listener);
        }
    }

    // Controller
    class CalculatorController {
        private CalculatorModel model;
        private CalculatorView view;

        public CalculatorController(CalculatorModel model, CalculatorView view) {
            this.model = model;
            this.view = view;

            this.view.addAddButtonListener(new AddButtonListener());
        }

        class AddButtonListener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                int userInput = view.getUserInput();
                model.add(userInput);
                view.setResult(model.getResult());
            }
        }
    }

    // Main class to run the application
    public class CalculatorApp {
        public static void main(String[] args) {
            CalculatorModel model = new CalculatorModel();
            CalculatorView view = new CalculatorView();
            CalculatorController controller = new CalculatorController(model, view);

            view.setSize(200, 150);
            view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            view.setVisible(true);
        }
    }
    ```

### Event Handling in Swing
25. **Explain the event handling mechanism in Swing. Provide an example demonstrating the use of an ActionListener in Swing.**

    Event handling in Swing involves registering event listeners to respond to user interactions. An ActionListener, for example, is used to handle button clicks.

    Example:

    ```java
    import javax.swing.*;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;

    public class SwingEventHandlingExample {
        public static void main(String[] args) {
            JFrame frame = new JFrame("Event Handling Example");

            JButton button = new JButton("Click Me");
            button.addActionListener(new MyActionListener());

            frame.getContentPane().add(button);
            frame.setSize(300, 200);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        }

        static class MyActionListener implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Button Clicked!");
            }
        }
    }
    ```

### Swing Layout Managers
26. **Discuss the concept of Swing Layout Managers. Provide examples of at least two different layout managers in Swing.**

    Layout Managers in Swing control the placement and sizing of components in a container.

    Example with BorderLayout:

    ```java
    import javax.swing.*;
    import java.awt.*;

    public class BorderLayoutExample {
        public static void main(String[] args) {
            JFrame frame = new JFrame("BorderLayout Example");

            JButton northButton = new JButton("North");
            JButton southButton = new JButton("South");
            JButton eastButton = new JButton("East");
            JButton westButton = new JButton("West");
            JButton centerButton = new JButton("Center");

            frame.setLayout(new BorderLayout());
            frame.add(northButton, BorderLayout.NORTH);
            frame.add(southButton, BorderLayout.SOUTH);
            frame.add(eastButton, BorderLayout.EAST);
            frame.add(westButton, BorderLayout.WEST);
            frame.add(centerButton, BorderLayout.CENTER);

            frame.setSize(300, 200);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        }
    }
    ```

    Example with GridLayout:

    ```java
    import javax.swing.*;
    import java.awt.*;

    public class GridLayoutExample {
        public static void main(String[] args) {
            JFrame frame = new JFrame("GridLayout Example");

            JButton button1 = new JButton("Button 1");
            JButton button2 = new JButton("Button 2");
            JButton button3 = new JButton("Button 3");
            JButton button4 = new JButton("Button 4");
            JButton button5 = new JButton("Button 5");

            frame.setLayout(new GridLayout(2, 3));
            frame.add(button1);
            frame.add(button2);
            frame.add(button3);
            frame.add(button4);
            frame.add(button5);

            frame.setSize(300, 200);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        }
    }
    ```
27. **Explain the Java Database Connectivity (JDBC) framework and its purpose.**

JDBC is a Java-based API that enables Java applications to interact with relational databases. It provides a standardized interface for connecting to databases, executing SQL queries, and handling results. The primary purpose is to facilitate seamless communication between Java applications and database systems.

28. **What are the key components of the JDBC framework, and how do they contribute to database interactions?**

- **DriverManager:** Manages database drivers and establishes connections.
- **Driver:** Handles communication with a specific database type.
- **Connection:** Represents a connection to the database.
- **Statement:** Executes SQL queries.
- **ResultSet:** Represents the result of a query, allowing data retrieval.

29. **How can you create a table in a relational database using JDBC? Provide an example.**

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateTableExample {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/your_database", "username", "password");
            Statement statement = connection.createStatement();

            String createTableQuery = "CREATE TABLE example_table (id INT PRIMARY KEY, name VARCHAR(50))";
            statement.executeUpdate(createTableQuery);

            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

30. **How do you delete records from a database using JDBC? Provide an example.**

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DeleteRecordsExample {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/your_database", "username", "password");
            Statement statement = connection.createStatement();

            String deleteQuery = "DELETE FROM example_table WHERE id = 1";
            statement.executeUpdate(deleteQuery);

            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

31. **How can you insert data into a database using JDBC? Provide an example.**

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class InsertDataExample {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/your_database", "username", "password");
            Statement statement = connection.createStatement();

            String insertQuery = "INSERT INTO example_table (id, name) VALUES (1, 'John')";
            statement.executeUpdate(insertQuery);

            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

32. **How do you select data from a database using JDBC? Provide an example.**

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SelectDataExample {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/your_database", "username", "password");
            Statement statement = connection.createStatement();

            String selectQuery = "SELECT * FROM example_table";
            ResultSet resultSet = statement.executeQuery(selectQuery);

            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                System.out.println("ID: " + id + ", Name: " + name);
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

34. **What is the purpose of the ResultSet interface in JDBC?**

    - The `ResultSet` interface represents the result set of a query, allowing the retrieval of data from the database. It provides methods to navigate through the result set and retrieve data based on column names or indices. The `ResultSet` is essential for processing and displaying query results.

35. **How does the `Statement` interface in JDBC contribute to executing SQL queries?**

    - The `Statement` interface in JDBC is used to execute SQL queries. It can handle both simple queries (`Statement`) and pre-compiled queries (`PreparedStatement`). The `Statement` interface provides methods like `executeQuery` for SELECT queries and `executeUpdate` for INSERT, UPDATE, DELETE queries.

36. **Explain the significance of the `PreparedStatement` interface in JDBC.**

    - The `PreparedStatement` interface extends `Statement` and is used to execute pre-compiled SQL queries. It offers benefits such as improved performance, better security by preventing SQL injection, and the ability to reuse the same query with different parameter values.

37. **How do you handle exceptions in JDBC when working with database operations?**

    - Exception handling in JDBC is crucial to manage errors that may occur during database interactions. It involves using `try-catch` blocks to catch exceptions such as `SQLException`. Proper exception handling ensures that the application gracefully handles database-related errors and provides meaningful feedback to developers and users.
38. **Here's a Java program using Swing that creates a JFrame with three text fields, three labels, and a button. The program takes a number as input in the first text field and, upon clicking the button, displays the previous and next numbers in the second and third text fields, respectively.

    ```java
    import javax.swing.*;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;
    
    public class NumberManipulationApp {
    
        public static void main(String[] args) {
            SwingUtilities.invokeLater(() -> {
                createAndShowGUI();
            });
        }
    
        private static void createAndShowGUI() {
            JFrame frame = new JFrame("Number Manipulation App");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
            // Create components
            JTextField inputTextField = new JTextField();
            JTextField previousTextField = new JTextField();
            JTextField nextTextField = new JTextField();
            JLabel inputLabel = new JLabel("Enter a Number:");
            JLabel previousLabel = new JLabel("Previous Number:");
            JLabel nextLabel = new JLabel("Next Number:");
            JButton manipulateButton = new JButton("Manipulate");
    
            // Set layouts
            frame.setLayout(null);
    
            // Set bounds for components
            inputLabel.setBounds(20, 20, 120, 30);
            inputTextField.setBounds(150, 20, 100, 30);
            manipulateButton.setBounds(270, 20, 120, 30);
            previousLabel.setBounds(20, 70, 120, 30);
            previousTextField.setBounds(150, 70, 100, 30);
            nextLabel.setBounds(20, 120, 120, 30);
            nextTextField.setBounds(150, 120, 100, 30);
    
            // Add components to the frame
            frame.add(inputLabel);
            frame.add(inputTextField);
            frame.add(manipulateButton);
            frame.add(previousLabel);
            frame.add(previousTextField);
            frame.add(nextLabel);
            frame.add(nextTextField);
    
            // Button action listener
            manipulateButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        // Get the input number
                        int inputNumber = Integer.parseInt(inputTextField.getText());
    
                        // Manipulate and display previous and next numbers
                        previousTextField.setText(String.valueOf(inputNumber - 1));
                        nextTextField.setText(String.valueOf(inputNumber + 1));
                    } catch (NumberFormatException ex) {
                        JOptionPane.showMessageDialog(frame, "Please enter a valid number.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
    
            // Set frame properties
            frame.setSize(400, 200);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        }
    }
    ```
39. Discuss the event handling mechanism in Java using the Delegation Event Model?    

    The Delegation Event Model is the event handling mechanism used in Java Swing, which is part of the Java Foundation Classes (JFC). It is a way to handle events in a component-based graphical user interface (GUI) system like Swing. The Delegation Event Model is based on the Observer design pattern, where components register interest in events, and listeners are notified when events occur.

    Here are the key components and concepts of the Delegation Event Model:
    
    1. **Event Source (or Event Producer):**
       - An event source is an object that generates events. In Swing, components like buttons, text fields, and others are examples of event sources.
       - These components have methods to register listeners for specific events. For example, a button has `addActionListener()` for action events.
    
    2. **Event Object:**
       - An event object encapsulates information about an event. It is an instance of a class that inherits from `java.util.EventObject` or one of its subclasses.
       - The event object carries details about the event, such as the source, type of event, and any other relevant information.
    
    3. **EventListener (or Event Consumer):**
       - An EventListener is an interface that defines methods to handle specific types of events.
       - In Swing, various listener interfaces exist, such as `ActionListener`, `MouseListener`, `KeyListener`, etc. Each interface corresponds to a specific type of event.
    
    4. **Event Registration:**
       - An event source allows other objects (listeners) to register interest in specific types of events. This is typically done through methods like `addActionListener()`, `addMouseListener()`, etc.
       - Multiple listeners can register for the same type of event from a single event source.
    
    5. **Delegation:**
       - When an event occurs, the event source delegates the responsibility of handling the event to the registered listeners.
       - The source notifies the registered listeners by invoking their appropriate event-handling methods. For example, calling the `actionPerformed(ActionEvent e)` method for an `ActionListener`.
    
    6. **Event Dispatch Thread (EDT):**
       - Swing components are not thread-safe, and all GUI updates should be done on the Event Dispatch Thread (EDT).
       - Event listeners, when notified of an event, are executed on the EDT to ensure proper synchronization and avoid concurrency issues.
    
    Here's a simple example demonstrating the use of the Delegation Event Model with a `JButton`:
    
    ```java
    import javax.swing.*;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;
    
    public class EventHandlingExample {
        public static void main(String[] args) {
            SwingUtilities.invokeLater(() -> {
                createAndShowGUI();
            });
        }
    
        private static void createAndShowGUI() {
            JFrame frame = new JFrame("Event Handling Example");
            JButton button = new JButton("Click Me");
    
            // Register an ActionListener to the button
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // This method is called when the button is clicked
                    JOptionPane.showMessageDialog(frame, "Button Clicked!");
                }
            });
    
            frame.getContentPane().add(button);
            frame.setSize(300, 200);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        }
    }
    ```
39. What are the uses of synchronized keyword in Java? Explain with examples.    

    The `synchronized` keyword in Java is used to control access to critical sections of code by multiple threads. It ensures that only one thread at a time can execute a synchronized method or a block of code. This is essential for preventing data corruption and maintaining the consistency of shared resources in a multithreaded environment. Here are some common use cases for the `synchronized` keyword:

    1. **Method Synchronization:**
       When a method is declared as synchronized, only one thread can execute that method at any given time. Other threads attempting to access the synchronized method have to wait until the current thread releases the lock.
    
        ```java
        public class SynchronizedExample {
    
            private int count = 0;
    
            // Synchronized method
            public synchronized void increment() {
                count++;
            }
    
            public static void main(String[] args) {
                SynchronizedExample example = new SynchronizedExample();
    
                // Multiple threads trying to increment the count
                for (int i = 0; i < 5; i++) {
                    new Thread(() -> {
                        for (int j = 0; j < 10000; j++) {
                            example.increment();
                        }
                    }).start();
                }
    
                // Wait for all threads to finish
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
    
                System.out.println("Count: " + example.count);
            }
        }
        ```
    
    2. **Block Synchronization:**
       Instead of synchronizing an entire method, you can use a synchronized block to protect a specific section of code. This allows more fine-grained control over synchronization.
    
        ```java
        public class SynchronizedBlockExample {
    
            private int count = 0;
            private Object lock = new Object();
    
            public void increment() {
                // Synchronized block
                synchronized (lock) {
                    count++;
                }
            }
    
            public static void main(String[] args) {
                SynchronizedBlockExample example = new SynchronizedBlockExample();
    
                // Multiple threads trying to increment the count
                for (int i = 0; i < 5; i++) {
                    new Thread(() -> {
                        for (int j = 0; j < 10000; j++) {
                            example.increment();
                        }
                    }).start();
                }
    
                // Wait for all threads to finish
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
    
                System.out.println("Count: " + example.count);
            }
        }
        ```
    
    3. **Static Method Synchronization:**
       Similar to instance methods, you can also use the `synchronized` keyword with static methods. In this case, the lock is associated with the class rather than an instance of the class.
    
        ```java
        public class SynchronizedStaticExample {
    
            private static int count = 0;
    
            // Synchronized static method
            public static synchronized void increment() {
                count++;
            }
    
            public static void main(String[] args) {
                SynchronizedStaticExample example1 = new SynchronizedStaticExample();
                SynchronizedStaticExample example2 = new SynchronizedStaticExample();
    
                // Multiple threads trying to increment the count using different instances
                for (int i = 0; i < 5; i++) {
                    new Thread(() -> {
                        for (int j = 0; j < 10000; j++) {
                            example1.increment();
                            example2.increment();
                        }
                    }).start();
                }
    
                // Wait for all threads to finish
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
    
                System.out.println("Count: " + count);
            }
        }
        ```
    In all these examples, the `synchronized` keyword ensures that the shared resource (`count` variable in this case) is accessed by only one thread at a time, preventing potential race conditions and ensuring data consistency. It's important to use synchronization judiciously to avoid performance issues and deadlocks.

40. Write java code to create three threads using runnable interface.

    ```java
    public class ThreeThreadsExample {
    
        public static void main(String[] args) {
            // Creating instances of the Runnable implementations
            RunnableTask task1 = new RunnableTask("Thread 1");
            RunnableTask task2 = new RunnableTask("Thread 2");
            RunnableTask task3 = new RunnableTask("Thread 3");
    
            // Creating threads and passing the Runnable instances
            Thread thread1 = new Thread(task1);
            Thread thread2 = new Thread(task2);
            Thread thread3 = new Thread(task3);
    
            // Starting the threads
            thread1.start();
            thread2.start();
            thread3.start();
        }
    
        // Runnable implementation
        static class RunnableTask implements Runnable {
            private String threadName;
    
            public RunnableTask(String threadName) {
                this.threadName = threadName;
            }
    
            @Override
            public void run() {
                for (int i = 1; i <= 5; i++) {
                    System.out.println(threadName + ": Count " + i);
                    try {
                        // Adding a short delay for demonstration purposes
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    ```
    - The `RunnableTask` class implements the `Runnable` interface and provides the `run()` method, which contains the code to be executed by the thread.
    - Three instances of `RunnableTask` are created, each representing a separate task to be executed concurrently.
    - Three `Thread` instances are created, passing the respective `RunnableTask` instances to their constructors.
    - The `start()` method is called on each thread to initiate the execution of their corresponding `run()` methods concurrently.
    


# ശുഭം
